package com.thanawat.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.thanawat.bmi.databinding.ActivityMainBinding
import java.text.NumberFormat
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener{ calculateBMI() }
    }

    private fun calculateBMI() {
        val stringWeight = binding.weight.text.toString()
        val stringHigh = binding.high.text.toString()
        if(stringHigh.trim().length<=0||stringWeight.trim().length<=0){
            Toast.makeText(this@MainActivity, "Please input data!", Toast.LENGTH_SHORT).show()
            return
        }
        else {
            val weight = stringWeight.toDouble()
            var high = stringHigh.toDouble()
            high /= 100
            val bmi = weight / (high * high)
            val formattedBMI = (bmi * 100.0).roundToInt() / 100.0
            var category = when {
                bmi < 18.5 -> "Under Weight (ผอม)"
                bmi in 18.5..22.9 -> "Normal (สมส่วน)"
                bmi in 23.0..24.9 -> "Over Weight (น้ำหนักเกิน)"
                bmi in 25.0..29.9 -> "Obese (สภาวะอ้วน)"
                else -> "Extremly Obese (สภาวะอ้วนมาก)"
            }
            var img = when {
                bmi < 18.5 -> R.drawable.under
                bmi in 18.5..22.9 -> R.drawable.normal
                bmi in 23.0..24.9 -> R.drawable.over
                bmi in 25.0..29.9 -> R.drawable.obese
                else -> R.drawable.extreme
            }
            binding.bmi.text = "BMI: ${formattedBMI} kg/m2"
            binding.category.text = "Category: ${category}"
            binding.imageView.setImageResource(img)
        }
    }


}